Lmod Training
==============


The Lmod training is a hands-on tutorial of Lmod and how to write Lua modules. This training is based off https://github.com/shahzebsiddiqui/Training_Material/tree/master/lmod with slight customizations to suite NERSC audience. 

In this session we will cover the following topics

 - Overview of `module` and `ml` command such as `module avail`, `module load`, `module swap`, `module purge`
 - Working with module collections - `module save`, `module describe`, `module restore`
 - Lmod `spider` utility 
 - Writing Lua Modules and covering some of the `Lua functions <https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html>`_


Getting Started
----------------

We have a Lmod Docker container - `shahzebsiddiqui/lmod <https://hub.docker.com/repository/docker/shahzebsiddiqui/lmod>`_ on Dockerhub 
which we will use for the training.  If you want to get started, you can run this training on your laptop assuming you have a `docker installation <https://docs.docker.com/engine/install/>`_.

If you are using `docker` you need to run the following to get an interactive shell in the container::

    docker run -it shahzebsiddiqui/lmod

If you don't have docker, you can `Login to Cori <https://docs.nersc.gov/connect/>`_ and use `shifter <https://docs.nersc.gov/development/shifter/how-to-use/>`_ to startup an interactive shell by running::  

    shifter --image=shahzebsiddiqui/lmod -E bash -l


Once you are in your container, check your **module** command and confirm you have the following::

        bash-4.2$ type module
        module is a function
        module () 
        { 
            eval $($LMOD_CMD bash "$@") && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
        }
        bash-4.2$ module --version

        Modules based on Lua: Version 8.3.1  2020-02-16 19:46 :z
            by Robert McLay mclay@tacc.utexas.edu


Next, you should clone this repo via `git` and source the setup script as follows::

  git clone https://gitlab.com/NERSC/lmod-training.git
  cd lmod-training
  source setup.sh

Once you are set, you can start the exercise

- `exercise1.rst <https://gitlab.com/NERSC/lmod-training/-/blob/master/exercise1.rst>`_ - Intro to Module Part 1
- `exercise2.rst <https://gitlab.com/NERSC/lmod-training/-/blob/master/exercise2.rst>`_ - Intro to Module Part 2
- `exercise3.rst <https://gitlab.com/NERSC/lmod-training/-/blob/master/exercise3.rst>`_ - Intro to Module File Part 1
- `exercise4.rst <https://gitlab.com/NERSC/lmod-training/-/blob/master/exercise4.rst>`_ - Intro to Module File Part 2


References
-----------

- Lmod Documentation: https://lmod.readthedocs.io/en/latest/index.html
- Lmod GitHub Project: https://github.com/TACC/Lmod
- Docker Container Repo: https://github.com/shahzebsiddiqui/lmod 
