Exercise 4
==========


Example: Translate modulefile from TCL to LUA
----------------------------------------------

The **zsh/5.4.2** is a TCL module and we can use the script ``tcl2lua.tcl`` for converting modulefile 
from TCL to LUA. For this example we will run the following::
   
        bash-4.2$ $LMOD_DIR/tcl2lua.tcl $ROOT/modules/ex2/zsh/5.4.2               
        whatis([===[Zsh is a shell designed for interactive use, although it is also a powerful scripting language. Many of the useful features of bash, ksh, and tcsh were incorporated into zsh; many original features were added.  ]===])
        conflict("zsh")
        prepend_path{"PATH","/global/common/sw/cray/sles15/x86_64/zsh/5.4.2/gcc/8.2.0/i4nhbsf/bin",delim=":",priority="0"}
        prepend_path{"MANPATH","/global/common/sw/cray/sles15/x86_64/zsh/5.4.2/gcc/8.2.0/i4nhbsf/share/man",delim=":",priority="0"}
        prepend_path{"CMAKE_PREFIX_PATH","/global/common/sw/cray/sles15/x86_64/zsh/5.4.2/gcc/8.2.0/i4nhbsf/",delim=":",priority="0"}

This script can be used for converting your modulefiles to Lua however the script is not perfect. For more details 
see `Converting TCL environment modules to Lmod <https://lmod.readthedocs.io/en/latest/073_tmod_to_lmod.html>`_. 

Example: Convert shell script to modulefile
--------------------------------------------

In this example we have a shell script **shell_to_lua.sh** which we want to 
convert to lua file. You can use the ``sh_to_modulefile`` utility to show content
of generated lua file. For more options run ``sh_to_modulefile --help``. 
::

        bash-4.2$ $LMOD_DIR/sh_to_modulefile shell_to_lua.sh 
        setenv("FOO","BAR")
        setenv("HELLO","WORLD")
        prepend_path("LD_LIBRARY_PATH","/.local")
        prepend_path("PATH","/.local")

To learn more about ``sh_to_modulefile`` please see https://lmod.readthedocs.io/en/latest/260_sh_to_modulefile.html


Example: Customize default module using .version file 
----------------------------------------------------

Check content of file ``$ROOT/modules/ex2/gcc/.version``. This file can be used to set default version for modulefile. A **.version** file can be 
set in the same directory where modulefiles are stored to control defaults. 

.. code::

        ml purge
        ml gcc


In LMOD, we can configure defaults for all module in a single file defined by variable **LMOD_MODULERCFILE** or **MODULERC** which points to a lmod configuration
file that can configure defaults, marking alias and hiding modules. To learn more see https://lmod.readthedocs.io/en/latest/093_modulerc.html.  

Example: Declaring sticky modules 
----------------------------------

The ``gcc/6.0`` is a sticky module that has ``add_property("lmod", "sticky")`` set in modulefile. This prevents ``module purge`` from removing sticky module. This particularly useful when setting startup modules that set MODULEPATH to other trees and we don't want user environment to be corrupted due to **module purge**. To purge sticky module you need to specify ``module --force purge``.

.. code::

        ml purge
        ml gcc/6.0
        # can't remove sticky module
        ml purge
        # try 
        ml --force purge

Example: Family modules
------------------------
To assign modules to Lmod Family you can use ``family("<name>")`` where **name** is the family name. Typically you want to setup families for like modules such as compilers, MPI, mathlibs (blas, mkl, lapack). Lmod can only load one member module from the family at given time. Lmod families prevent users from corrupting their user environment,  for instances a user loads two different  MPI providers such as `openmpi` and `mpich` that both provide compiler wrappers ``mpicc``. This can lead to unexpected result since order of ``module load`` matter and depending on how your modulefiles are written. In the end, Linux will check **$PATH** and pick up the first instance of **mpicc** in your directory tree which may not be what you expect.

.. code::

        # gcc and pgi should automatically swap
        ml purge
        ml gcc
        echo $CC
        ml pgi
        echo $CC

Example: pushenv example
---------------------------

The **pushenv** function can be used to set environment similar to **setenv** function except it can save its state when other modules set the same variable. It's equivalent push and pop items from a stack,
upon ``module load`` items are pushed  and items are popped from stack upon ``module unload``. To demonstrate, we have ``pgi`` and ``openmpi/2.0.0-pgi-17.10`` module both set ``pushenv("CC", "pgcc")`` and ``pushenv("CC", "mpicc")``. When you load
``pgi`` module it will set CC to *pgcc*, but when you load openmpi it will override CC to *mpicc* however when you unload openmpi it will set CC back to *pgcc* because pushenv retains a history of environment. The **pushenv** function is useful
for declaring compiler wrappers to a common wrapper (cc, fc, cxx) often used in Makefiles. You can set all your compiler and MPI wrappers with ``pushenv`` if you want to refer to them via variables cc, fc, cxx in your Makefile.

.. code::

        ml purge
        ml pgi
        echo $CC
        ml openmpi/2.0.0-pgi-17.10
        echo $CC
        ml -openmpi/2.0.0-pgi-17.10
        echo $CC

Example: Specify Module Range Criteria
--------------------------------------

If you want to define a range of versions as search criteria for loading a module you can use ``between`` function. In this example we define **openmpi** module to load gcc version 6.0-6.9.


.. code::

        ml purge
        ml openmpi/2.1.0-gcc-6.x
        ml

Example: group module example
-------------------------------

In this example, we define the ``Go`` module that can only be loaded by a specific unix group in this case users of **nstaff** can load this module. This is useful for licensed software particularly gaussian, or vasp that require
software to be loaded by a specific group. You can use **userInGroup** function to control which user group can load this module. Try changing the user group to a different name that you don't belong to and rerun command.


.. code::

        ml purge
        ml Go/1.9
